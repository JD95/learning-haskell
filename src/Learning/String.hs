{-# LANGUAGE FlexibleInstances #-}

module Learning.String where

import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as BS
import Data.Text (Text)
import qualified Data.Text as Text

class StringCast s where
  toString :: s -> String
  toText :: s -> Text
  toByteString :: s -> ByteString

instance StringCast [Char] where
  toString = id
  toText = Text.pack
  toByteString = BS.pack

instance StringCast Text where
  toString = Text.unpack
  toText = id
  toByteString = BS.pack . Text.unpack

instance StringCast ByteString where
  toString = BS.unpack
  toText = Text.pack . BS.unpack
  toByteString = id
