module Learning.IO.Loops where

whileIO :: (st -> Bool) -> (st -> IO st) -> (st -> IO st)
whileIO cond step = loop where
  loop st = do
    if cond st
      then loop =<< step st
      else pure st

doWhileIO :: (st -> Bool) -> (st -> IO st) -> (st -> IO st)
doWhileIO cond step st =
  whileIO cond step =<< step st

forIO ::
  i ->
  (i -> Bool) ->
  (i -> i) ->
  (i -> st -> IO (i, st)) ->
  (st -> IO st)
forIO init cond inc body =
  fmap snd . loop init
  where
  loop i st
    | cond i = do
        (i', st') <- body i st
        loop (inc i') st'
    | otherwise = pure (i, st)

iterIO ::
  i ->
  (i -> Bool) ->
  (i -> i) ->
  (i -> st -> IO st) ->
  (st -> IO st)
iterIO init cond inc body =
  forIO init cond inc (\i st -> (,) i <$> body i st)
