module Learning.IO where

asIO :: a -> IO a
asIO = pure

getValidInput :: (String -> Either String a) -> IO a
getValidInput validate = loop where
  loop = do
    input <- getLine
    case validate input of
      Right result ->
        asIO result
      Left errMsg -> do
        putStrLn errMsg
        loop
