module Learning.Text where

split :: Char -> String -> [String]
split c [] = []
split c xs =
  let (chunk, rest) = break (c ==) xs
  in chunk : split c (drop 1 rest)
