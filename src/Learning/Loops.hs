module Learning.Loops where

while :: (st -> Bool) -> (st -> st) -> (st -> st)
while cond step = loop where
  loop st =
    if cond st
      then loop (step st)
      else st

doWhile :: (st -> Bool) -> (st -> st) -> (st -> st)
doWhile cond step =
  while cond step . step

for ::
  i ->
  (i -> Bool) ->
  (i -> i) ->
  (i -> st -> (i, st)) ->
  (st -> st)
for init cond inc step = snd . loop init where
  loop i st
    | cond i =
      let (i', st') = step i st
      in loop (inc i') st'
    | otherwise = (i, st)

iter ::
  i ->
  (i -> Bool) ->
  (i -> i) ->
  (i -> st -> st) ->
  (st -> st)
iter init cond inc step =
  for init cond inc (\i st -> (i, step i st))
